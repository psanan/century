#!/usr/bin/env bash
# This requires ImageMagick

OUTFILE=montage.png

# All images should ideally be at least this big in their
#  smallest dimension
DIM=300 

# $SOURCE is supposed to contain files 1.png to 100.png
SOURCE=source

WORK=work
mkdir -p $WORK

echo -n "Processing "
for i in {1..100}
do
  echo -n "$i.."
  convert "$SOURCE"/$i.png -resize "$DIM"x"$DIM"^ -extent "$DIM"x"$DIM" -gravity center "$WORK"/"$i"_resized.png
done
echo Done.

echo -n Creating Montage...
montage "$WORK"/{1..100}_resized.png -geometry +0+0 "$OUTFILE"
echo Done.

rm "$WORK"/{1..100}_resized.png
rmdir "$WORK"

echo Output in "$OUTFILE"
