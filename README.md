Century
=======
v2.0.0

A playlist, with 1-minute segments specified.

Thanks to all the friends who helped me with this!

Copyright Patrick Sanan.
Commercial or for-profit reuse (including posting on social media, anywhere related to streaming music services, or websites with advertising)
is prohibited without prior explicit permission.

Rules
-----

* There are 100 tracks, "Track 1" to "Track 100"
* Track N must mention the number "N", in English, at least 1 minute before the end of the track
* An additional track is specified for the first minute of a mix
* Bonus: A track must be included from each decade of the past century

With these, a mix can be constructed such that Track N mentions "N" when exactly N minutes have elapsed.

Track N is selected preferring the following, approximately ordered from most to least important.

* Being an iconic track about N
* Being somehow "about" N
* Being good, interesting, or funny in my opinion
* Mentioning N clearly
* Mentioning N in the first line or in the chorus
* Having N in the title
* Mentioning N many times
* Not mentioning other numbers
* Being by a notable artist
* Being on an officially released recording
* Being sung, not spoken
* Being the original recorded version (as opposed to an edit, live version, remix, or cover)
* Not introducing too many tracks by the same artist, era, or style into the mix
* Being an upbeat track
* Being a "popular" track (as opposed to, say, choral or experimental music)

Tracks
------
````text
N   Artist            Title                    Release (Year)                    Mention
..........................................................................................
-   Live              Century                  Secret Samadhi (1997)             0:00

1   Harry Nilsson     One                      Aerial Pandemonium Ballet (1968)  0:08

2   The Toyes         Smoke Two Joints         The Toyes (1996)                  0:25

3   Blind Melon       Three Is a Magic Number  Schoolhouse Rock! Rocks (1996)    0:06

4   Cake              Friend is a Four Letter  Fashion Nugget (1996)             0:19
                       Word

5   The Luniz         I Got 5 on It            Operation Stackola (1995)         1:29

6   Sneaker Pimps     6 Underground            Becoming X (1996)                 0:36

7   Flogging Molly    Seven Deadly Sins        Within a Mile of Home (2004)      0:09

8   The Beatles       Eight Days a Week        Beatles For Sale (1964)           1:00

9   Rammstein         Du Hast                  Sennsucht (1997)                  1:42

10  M.I.A.            10 Dollar                Arular (2005)                     2:40

11  Momus             Eleven Executioners      The Poison Boyfriend (1987)       0:15

12  Venetian Snares   Twelve                   Infolepsy (2005)                  1:43

13  Pixies            No. 13, Baby             Doolittle (1989)                  0:27

14  Stars             14 Forever               Sad Robots (2008)                 0:42

15  Rock Central      Fifteen Hands            Are We Not Horses (2006)          1:48
     Plaza

16  Merle Travis      Sixteen Tons             Sixteen Tons (1947)               1:05

17  Ladytron          Seventeen                Light And Magic (2002)            0:20

18  Alice Cooper      I'm Eighteen             Love It to Death (1971)           0:45

19  Paul Hardcastle   19                       Paul Hardcastle (1985)            0:16

20  The Misfits       20 Eyes                  Walk Among Us (1982)              0:05

21  The Who           1921                     Tommy (1969)                      0:25

22  Taylor Swift      22                       Red (2012)                        1:20

23  Blonde Redhead    23                       23 (2007)                         0:23

24  The Ramones       I Wanna Be Sedated       Road to Ruin (1978)               0:06

25  Witch Hunt        Twenty-Five              Blood Red States (2006)           0:10

26  The Four Preps    26 Miles (Santa          The Four Preps (1957)             0:05
                       Catalina)

27  Young Fathers     27                       White Men Are Black Men Too       0:33
                                                (2015)

28  Steppenwolf       28                       The Second (1968)                 0:27

29  Robert Plant      29 Palms                 Fate of Nations (1993)            1:42

30  Scott Walker      30 Century Man           Scott 3 (1969)                    0:26

31  The Shirelles     31 Flavors               It's a Mad, Mad, Mad, Mad World   0:10
                                                (1963)

32  They Might Be     32 Footsteps             They Might Be Giants (1986)       0:08
     Giants

33  Okkervil River    He Passes Number 33      Julie Doiron / Okkervil River     4:38

34  Charley Patton    34 Blues                 Poor Me / 34 Blues (1934)         0:06

35  A.C. Newman       35 in the Shade          Slow Wonder (2004)                0:36

36  John Cooper       36 Hours                 Snap, Crackle, and Bop (1980)     0:04
     Clarke

37  Devo              "37"                     Hardcore Devo: Volume 2 (1991)    0:36

38  Revolting Cocks   38                       Big Sexy Land (1986)              2:20

39  Queen             '39                      A Night at the Opera (1975)       0:36

40  E-40              40 Water                 The Ball Street Journal (2008)    0:34

41  Boozoo Chavis     Forty-One Days           Forty-One Days (1955)             0:18

42  Dan Bern          42                       Doubleheader (2012)               0:52

43  E-40              43                       Revenue Retrievin' : Graveyard    1:11
                                                Shift (2011)

44  Howlin' Wolf      Forty Four               "Forty Four" / I'll be Around     0:29
                                                (1954)

45  Cornershop        Brimful of Asha          When I Was Born for the Seventh   1:05
                                                Time (1997)

46  Tool              46 & 2                   Aenima (1996)                     3:25

47  Krayzie Bone      Heated Heavy             Thug Mentality (1999)             0:07

48  The Clash         48 Hours                 The Clash (1977)                  0:14

49  Royksopp          49 Percent               The Understanding (2005)          2:27

50  Paul Simon        50 Ways to Leave Your    Still Crazy After All These       0:48
                       Lover                    Years (1975)

51  Pixies            The Happening            Bossanova (1990)                  0:34

52  Buggles           Video Killed the Radio   The Age of Plastic (1980)         0:19
                       Star

53  The B-52's        53 Miles West of Venus   Wild Planet (1980)                2:50

54  Ivan & The        Room 54                  Mode Bizarre (2014)               0:01
     Parazol

55  Sammy Hagar       I Can't Drive 55         VOA (1984)                        2:13

56  Roy Payne         Back in '56              Last of the Reasons I Keep Going  0:22
                                                Home (1991)

57  Bruce             57 Channels (and         Human Touch (1992)                0:26
     Springsteen       Nothings' On)

58  Al Stewart        Class of '58             A Beach Full of Shells (2005)     3:01

59  Vernon Green &    Buick '59                Buick '59 / The Letter (1954)     0:22
     The Medallions

60  New Order         60 Miles an Hour         Get Ready (2001)                  1:56

61  Bob Dylan         Highway 61 Revisited     Highway 61 Revisited (1965)       0:36

62  Tim Easton        Highway 62 Love Song     Since 1966: Volume 1 (2011)       1:03

63  Frankie Valli     December, 1963           Who Loves You (1975)              0:19
     and the Four
     Seasons

64  The Beatles       When I'm Sixty Four      Sgt. Pepper's Lonely Hearts Club  0:36
                                                 Bands (1967)

65  John Grant        GMF                      Pale Green Ghosts (2013)          1:26

66  Depeche Mode      Route 66                 Behind the Wheel (2004)           0:40

67  Lord Kitchener    '67                      Kitch 67 (1966)                   0:50

68  The Alarm         Sixty Eight Guns         Declaration (1984)                1:00

69  Bryan Adams       Summer of '69            Reckless (1985)                   0:16

70  Red Ingle         (I Love You) For         Tim-Tayshun (1947)                0:11
                       Sentimental Mental
                       Reasons/(I Love You)
                       For Seventy Mental
                       Reasons

71  Electric Wizard   Torquemada 71            Witchcult Today (2001)            1:19

72  Dadawah           Seventy-Two Nations      Peace and Love (1974)             1:41

73  Blues Image       Ride Captain Ride        Open (1970)                       0:16

74  Robert Nighthawk  Seventy-Four             Bricks in my Pillow (1952)        0:55

75  Limousine         Seventy Five             Limousine (1975)                  0:47

76  The Dickies       The Spirit of '76        The Spirit of '76 (1991)          0:55

77  The Riptides      Sunset Strip             Tombs of Gold (2004)              0:38

78  Gruppo Sportivo   PS 78                    Back to 78 (1978)                 1:03

79  Tom Robinson      The Winter of '79        Power in the Darkness (1978)      1:01
     Band

80  Eve 6             Open Road Song           Eve 6 (1998)                      0:46

81  Mondo Rock        Summer of '81            Chemistry (1981)                  2:23

82  Miss Kittin &     1982                     First Album (2004)                1:23
     The Hacker

83  Irene Cara        Romance '83              What a Feelin' (1983)             0:28

84  Undead            In Eighty Four           Dawn of the Undead (1991)         0:36

85  Youngbloodz       85                       Against Da Grain (1999)           0:19

86  Green Day         '86                      Insomniac (1995)                  0:48

87  David Bowie       '87 and Cry              Never Let Me Down (1987)          0:33

88  Jackie Brenston   Rocket "88"              Rocket "88" (1951)                0:22
     & His Delta
     Cats

89  Sublime           89 Vision                Everything Under the Sun (2006)   0:16

90  Jane Jensen       Highway 90               Comic Book Whore (1996)           1:35

91  Dave Wilburn      Ninety-One Pounds of     Why Can't I Have You /
                       Lovin'                   Ninety-One Pounds of Lovin'      0:07
                                                (1963)

92  Fountains of      '92 Subaru               Traffic and Weather (1992)        0:52
     Wayne

93  Souls of          93 'til Infinity         93 'til Infinity (1993)           0:14
     Mischief

94  Will Fyffe        I'm 94 Today             I Belong to Glasgow / I'm 94      0:10
                                                Today (1957)

95  Shai              "95"                     Blackface (1995)                  0:37

96  ? and the         96 Tears                 96 Tears (1966)                   1:08
     Mysterians

97  Vernon Dalhart    Wreck of the Old '97     The Wreck of the Old 97 (1924)    0:53

98  Alanis Morissette Ironic                   Jagged Little Pill (1995)         0:15

99  Jay-Z             99 Problems              The Black Album (2003)            0:03

100 Sharon Jones and  100 Days, 100 Nights     100 Days, 100 Nights (2007)       0:16
     The Dap-Kings
````