#!/usr/bin/env bash
FILE="README.md"
for PREF in {192..201}
do
        printf "$PREF""0s: "
        COUNT=$(grep "($PREF" $FILE | wc -l)
        printf "$COUNT "
        for (( I=0; I<${COUNT}; I++ ))
        do
                printf "#"
        done
        printf "\n"
done
